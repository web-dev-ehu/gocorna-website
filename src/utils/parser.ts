export function parse(unparsedTemplate: string, data: Object) {
    function getValueByString(object: Object, variable: string): string {
        variable = variable.replace(/\[(\w+)\]/g, '.$1');
        variable = variable.replace(/^\./, '');
        const a = variable.split('.');
        for (let i = 0, n = a.length; i < n; ++i) {
            const k = a[i];
            if (k in object) {
                object = object[k];
            } else {
                console.assert(k in object, `Key ${k} is not in ${object}`);
            }
        }
        console.log(object);
        return object.toString();
    }

    return unparsedTemplate.replace(/\{\{.+?}}/gm, (variable: string) => {
        return getValueByString(
            data,
            variable.replace('{{', '').replace('}}', '')
        );
    });
}