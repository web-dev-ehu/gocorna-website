import './app-layout.css';
import { AppButton, TAppButtonProperties } from "../app-button/app-button";
import { AppHeading, TAppHeadingProperties } from "../app-heading/app-heading";

export interface ISectionContentProperties {
    headingProperties?: TAppHeadingProperties;
    buttonProperties?: TAppButtonProperties;
    text?: string;
    imagePath?: string;
}

export function AppLayout(properties: ISectionContentProperties): HTMLElement {
    const appLayout = document.createElement('div');
    appLayout.classList.add('app-layout');

    const appLayoutText = document.createElement('div');
    appLayoutText.classList.add('app-layout-text');

    if (properties.headingProperties) {
        const heading = AppHeading(properties.headingProperties);
        appLayoutText.appendChild(heading);
    }

    if (properties.text) {
        const text = document.createElement('p');
        text.innerHTML = properties.text;
        appLayoutText.appendChild(text);
    }

    if (properties.buttonProperties) {
        const sectionButton = AppButton(properties.buttonProperties);
        appLayoutText.appendChild(sectionButton);
    }

    appLayout.appendChild(appLayoutText);

    if (properties.imagePath) {
        const appLayoutImage = document.createElement('div');
        appLayoutImage.classList.add('app-layout-image');
        const image = document.createElement('img');
        image.classList.add('section-image');
        image.src = properties.imagePath;
        image.alt = 'section image';

        appLayoutImage.appendChild(image);
        appLayout.appendChild(appLayoutImage);
    }

    return appLayout;
}
