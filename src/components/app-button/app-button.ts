import './app-button.css';
import {TMode} from "../../types/TMode";

export type TAppButtonProperties = {
    mode: TMode;
    buttonText: string;
}


export function AppButton(properties: TAppButtonProperties): HTMLButtonElement {
    const host = document.createElement('button');

    host.innerText = properties.buttonText;
    host.classList.add('app-button');
    host.classList.add(`app-button--${properties.mode}`);

    return host;
}