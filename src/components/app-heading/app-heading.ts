import './app-heading.css';
import {TMode} from "../../types/TMode";

export type TAppHeadingProperties = {
    mode: TMode;
    headingText: string;
}


/**
 *
 * @description properties.headingText gets string with *(star symbol) to mark highlighted text
 * @param properties TAppHeadingProperties
 * @constructor
 */
export function AppHeading(properties: TAppHeadingProperties): HTMLDivElement {
    const { mode, headingText} = properties;
    const host = document.createElement('h2');
    host.classList.add('app-heading');

    host.innerHTML = headingText.replace(/\*.+?\*/gm, (match: string) => {
       return `<span>${match.slice(1, -1)}</span>`;
    });

    const coloredText = host.querySelector("span");
    coloredText.classList.add(`app-heading--${mode}`);

    return host;
}