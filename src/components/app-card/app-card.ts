import './app-card.css';

export type TAppCardProperties = {
    imgPath: string;
    title: string;
    text: string;
}


export function AppCard(properties: TAppCardProperties): HTMLDivElement {
    const host = document.createElement('div');
    host.classList.add('app-card');

    const icon = document.createElement('img');
    icon.src = properties.imgPath;
    icon.alt = 'icon';
    icon.classList.add('app-card-icon')
    host.appendChild(icon);

    const title = document.createElement("h4");
    title.innerText = properties.title;
    title.classList.add('app-card-title');
    host.appendChild(title);

    const text = document.createElement("p");
    text.innerText = properties.text;
    text.classList.add('app-card-text');
    host.appendChild(text);

    return host;
}
