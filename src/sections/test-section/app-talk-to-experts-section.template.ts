export const TALK_TO_EXPERTS_TEMPLATE: string = `
<div class="tte-counter">
    <div class="rect"></div>
    <div class="rect"></div>
    <div class="rect"></div>
    <div class="tte-counter-item">
        <div class="number">{{users}}</div>
        <div class="label">Users</div>
    </div>
    <div class="tte-counter-item">
        <div class="number">{{countries}}</div>
        <div class="label">Countries</div>
    </div>
    <div class="tte-counter-item">
        <div class="number">{{experts}}</div>
        <div class="label">Medical experts</div>
    </div>
</div>
<div class="tte-row" id="tte-row">
</div>
`;