import './app-talk-to-experts-section.css';
import {AppButton, TAppButtonProperties} from "../../components/app-button/app-button";
import {AppHeading, TAppHeadingProperties} from "../../components/app-heading/app-heading";
import { TALK_TO_EXPERTS_TEMPLATE } from './app-talk-to-experts-section.template'
import {parse} from "../../utils/parser";
import {AppLayout, ISectionContentProperties} from "../../components/app-layout/app-layout";
import talkImage from '../../assets/images/talk-image.png';

export type TTalkToExpertsSectionProperties = {
    button: TAppButtonProperties;
    title: TAppHeadingProperties;
    description: string;
    users: string;
    countries: string;
    experts: string;
}


export function AppTalkToExpertsSection(): HTMLElement {
    const host = document.createElement('section');
    const data: TTalkToExpertsSectionProperties = {
        title: {
            mode: "secondary",
            headingText: "Talk to <span>experts.</span>"
        },
        description: `
            Book appointments or submit queries into thousands of<br> 
            forums concerning health issues and prevention<br> 
            against noval Corona Virus.
        `,
        button: {
            mode: "primary",
            buttonText: "FEATURES"
        },
        countries: "78",
        experts: "10,000+",
        users: "2m"
    }

    host.id = 'talk-to-experts';
    host.classList.add('tte');
    host.innerHTML = parse(TALK_TO_EXPERTS_TEMPLATE, data);

    const layout = AppLayout({
        headingProperties: data.title,
        buttonProperties: data.button,
        text: data.description,
        imagePath: talkImage
    });

    host.querySelector('#tte-row').append(layout);

    return host;
}