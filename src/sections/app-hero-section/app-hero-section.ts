import './app-hero-section.css';
import { AppButton, TAppButtonProperties} from "../../components/app-button/app-button";
import { AppLayout, ISectionContentProperties} from "../../components/app-layout/app-layout";
import logoImgUrl from '../../assets/logo/logo.svg';
import watchImgUrl from '../../assets/icons/watch-btn.svg';
import heroImage from '../../assets/images/hero-image.svg';

export function AppHeroSection(): HTMLElement {
    const host = document.createElement('section');
    host.classList.add('hero-section');

    const container = document.createElement('div');
    container.classList.add('hero-container');


    const header = document.createElement('header');
    header.classList.add('hero-header');

    const logoLink = document.createElement('a');
    logoLink.href = '#';

    const logo = document.createElement('img');
    logo.src = logoImgUrl;
    logo.alt = 'Logo';

    logoLink.appendChild(logo);
    header.appendChild(logoLink);

    const nav = document.createElement('nav');
    const navList = document.createElement('ul');
    navList.classList.add('hero-nav-list');

    const navItems = ["HOME", "FEATURES", "SUPPORT", "CONTACT US"];

    navItems.forEach(itemText => {
        const listItem = document.createElement('li');
        const navItem = document.createElement('a');
        navItem.href = '#';
        navItem.textContent = itemText;
        listItem.append(navItem);
        navList.append(listItem);
    });

    nav.append(navList);
    header.append(nav);

    const buttonProperties: TAppButtonProperties = {
        mode: "secondary",
        buttonText: "DOWNLOAD",
    };

    const headerButton = AppButton(buttonProperties);
    header.append(headerButton);

    container.append(header);


    const sectionContentProperties: ISectionContentProperties = {
        headingProperties: {
            mode: "secondary",
            headingText: "Take care of your family’s *health.*",
        },
        buttonProperties: {
            mode: "primary",
            buttonText: "GET STARTED"
        },
        text: "All in one destination for COVID-19 health queries. Consult 10,000+ health workers about your concerns.",
        imagePath: heroImage
    }

    const mainLayout = AppLayout(sectionContentProperties)
    container.append(mainLayout);


    const watchLink = document.createElement('a');
    watchLink.href = '#';
    watchLink.classList.add('hero-watch-link');

    const watchLinkImg = document.createElement('img');
    watchLinkImg.src = watchImgUrl;
    watchLinkImg.alt = 'Watch';
    watchLink.append(watchLinkImg);

    const text1 = document.createElement('p');
    text1.textContent = "Stay safe with GoCorona";
    text1.classList.add('hero-watch-text1');

    const text2 = document.createElement('p');
    text2.textContent = "WATCH VIDEO";
    text2.classList.add('hero-watch-text2');

    const textContainer = document.createElement('div');
    textContainer.append(text1);
    textContainer.append(text2);
    watchLink.append(textContainer);
    container.append(watchLink);

    const rect = document.createElement('div');
    rect.classList.add('hero-rect');
    container.append(rect);

    host.append(container)

    return host;
}
