import './app-stay-section.css';
import { AppLayout, ISectionContentProperties} from "../../components/app-layout/app-layout";

import stayImg from '../../assets/images/stay-image-with-rect.png';

const sectionContentProperties: ISectionContentProperties = {
    headingProperties: {
        mode: "primary",
        headingText: "Stay safe with *GoCorona.*",
    },
    buttonProperties: {
        mode: "primary",
        buttonText: "FEATURES"
    },
    text: "24x7 Support and user friendly mobile platform to bring healthcare to your fingertips. Signup and be a part of the new health culture.",
    imagePath: stayImg,
}

export function AppStaySection(): HTMLElement {
    const host = document.createElement('section');
    host.classList.add('stay-section');

    const container = document.createElement('div');
    container.classList.add('stay-container');
    host.append(container);

    const mainLayout = AppLayout(sectionContentProperties);
    container.append(mainLayout);

    return host;
}