import './app-healthcare-section.css';
import humanImage from '../../assets/icons/human.svg';
import doctorImage from '../../assets/icons/doctor.svg';
import heartImage from '../../assets/icons/heart.svg';
import googleImage from '../../assets/images/google-banner.png';
import appstoreImage from '../../assets/images/appstore-banner.png';
import { AppLayout, ISectionContentProperties} from "../../components/app-layout/app-layout";
import { AppCard, TAppCardProperties} from "../../components/app-card/app-card";

const sectionContentProperties: ISectionContentProperties = {
    headingProperties: {
        mode: "primary",
        headingText: "*Healthcare* at your Fingertips.",
    },
    text: "Bringing premium healthcare features to your fingertips. User friendly mobile platform to bring healthcare to your fingertips. Signup and be a part of the new health culture."
}

const cardList: TAppCardProperties[] = [
    {
        imgPath: humanImage,
        title: 'Symptom Checker',
        text: 'Check if you are infected by COVID-19 with our Symptom Checker'
    },
    {
        imgPath: doctorImage,
        title: '24x7 Medical support',
        text: 'Consult with 10,000+ health workers about your concerns.'
    },
    {
        imgPath: heartImage,
        title: 'Conditions',
        text: 'Bringing premium healthcare features to your fingertips.'
    },
]



export function AppHealthcareSection(): HTMLElement {
    const host = document.createElement('section');
    host.classList.add('hc-section');

    const container = document.createElement('div');
    container.classList.add('hc-container');
    host.append(container);

    const mainLayout = AppLayout(sectionContentProperties);
    container.append(mainLayout);

    const cardsContainer = document.createElement('div');
    cardsContainer.classList.add('hc-cards');
    container.append(cardsContainer);

    cardList.forEach((item: TAppCardProperties) => {
        const card =  AppCard(item);
        card.classList.add('hc-card');
        cardsContainer.append(card);
    })

    const numList = [1,2,3,4];
    numList.forEach((num) => {
        const rect = document.createElement('div');
        rect.classList.add(`hc-rect-${num}`);
        cardsContainer.append(rect);
    })

    const links = document.createElement('div');
    links.classList.add(`hc-links`);
    container.append(links);

    const googleLink = document.createElement('a');
    googleLink.href = '#';
    googleLink.classList.add('hc-google-link');
    const googleLinkImg = document.createElement('img');
    googleLinkImg.src = googleImage;
    googleLinkImg.alt = 'GoogleStore';
    googleLink.append(googleLinkImg);
    links.append(googleLink);

    const appstoreLink = document.createElement('a');
    appstoreLink.href = '#';
    appstoreLink.classList.add('hc-appstore-link');
    const appstoreLinkImg = document.createElement('img');
    appstoreLinkImg.src = appstoreImage;
    appstoreLinkImg.alt = 'Appstore';
    appstoreLink.append(appstoreLinkImg);
    links.append(appstoreLink);






    return host;
}
