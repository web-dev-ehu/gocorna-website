import './app-test-section.css';
import {AppButton, TAppButtonProperties} from "../../components/app-button/app-button";

export function AppTestSection(): HTMLElement {
    const host = document.createElement('section');

    const textButtonProperties: TAppButtonProperties = {
        mode: 'primary',
        buttonText: 'Test button'
    };

    host.innerHTML = `
        <div class="header"></div>
        <div class="left-side"></div>
        <div class="right-side"></div>
    `;

    const header = host.querySelector('.header');
    const leftSide = host.querySelector('.left-side');
    const rightSide = host.querySelector('.right-side');

    leftSide.append(AppButton(textButtonProperties));

    return host;
}