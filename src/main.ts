import "./styles/fonts.css";
import "./styles/styles.css";

import {AppHeroSection} from "./sections/app-hero-section/app-hero-section";
import {AppHealthcareSection} from "./sections/app-healthcare-section/app-healthcare-section";
import {AppTalkToExpertsSection} from "./sections/test-section/app-talk-to-experts-section";
import {AppStaySection} from "./sections/app-stay-section/app-stay-section";

const app = document.querySelector("#app"); // div#app

const hero = AppHeroSection();
const stay = AppStaySection();
const talkToExperts = AppTalkToExpertsSection();
const healthcare = AppHealthcareSection()

app.append(hero);
app.append(stay);
app.append(talkToExperts);
app.append(healthcare);